﻿using Eco.Shared.Serialization;
using Eco.Gameplay.Skills;
using Eco.Shared.Localization;

namespace SkillJob.SkillsTools.SkillWorkerJob.PillMakerBase
{


    [Serialized]
    public partial class SkillWorker : Skill
    {
        public override string FriendlyName { get { return "SkillWorker"; } }
        public override string Description { get { return Localizer.DoStr("Improve skillpoint and other fun stuff."); } }
       
        public static int[] SkillPointCost = { 1, 1, 1, 1, 1 };
        public override int RequiredPoint { get { return this.Level < this.MaxLevel ? SkillPointCost[this.Level] : 0; } }
        public override int PrevRequiredPoint { get { return this.Level - 1 >= 0 && this.Level - 1 < this.MaxLevel ? SkillPointCost[this.Level - 1] : 0; } }
        public override int MaxLevel { get { return 1; } }
    }

}
