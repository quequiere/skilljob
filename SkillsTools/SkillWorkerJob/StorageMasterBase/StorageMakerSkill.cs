﻿using Eco.Shared.Serialization;
using Eco.Gameplay.Skills;
using Eco.Shared.Localization;
using SkillJob.SkillsTools.SkillWorkerJob.PillMakerBase;

namespace SkillJob.Skills
{

    [Serialized]
    [RequiresSkill(typeof(SkillWorker), 0)]
    public partial class StorageMakerSkill : Skill
    {
        public override string FriendlyName { get { return "StorageMaker"; } }

        public override string Description { get { return Localizer.DoStr(""); } }

        public static int[] SkillPointCost = { 1, 1, 1, 1, 1 };
        public override int RequiredPoint { get { return this.Level < this.MaxLevel ? SkillPointCost[this.Level] : 0; } }
        public override int PrevRequiredPoint { get { return this.Level - 1 >= 0 && this.Level - 1 < this.MaxLevel ? SkillPointCost[this.Level - 1] : 0; } }
        public override int MaxLevel { get { return 1; } }


    }






}
