﻿using System.Collections.Generic;
using System.Linq;
using Eco.Gameplay.Components;
using Eco.Core.Utils;
using Eco.Core.Controller;
using Eco.Gameplay.Objects;
using Eco.Shared;
using Eco.Shared.Serialization;
using Eco.Shared.Utils;
using Eco.Gameplay.Wires;
using Eco.Gameplay.Pipes.LiquidComponents;
using System;
using Eco.Gameplay.Items;

namespace SkillJob.SkillsTools.Items.Table
{
    [Serialized]
    class PipeCompoCustom : PipeComponent
    {
        private StatusElement status;

        public override void Initialize()
        {
            this.Tanks.ForEach(t => t.Initialize(this.Parent));
            this.status = this.Parent.GetComponent<StatusComponent>().CreateStatusElement();
        }

        public override void Tick()
        {
            this.Tanks.ForEach(x => x.Tick(WorldObjectManager.TickDeltaTime));
            this.status.SetStatusMessage(this.Enabled, "Output tank is not full", "Output tank is full");

            if (this.Parent.GetComponent<CraftingComponent>() != null)
            {
                var crafting = this.Parent.GetComponent<CraftingComponent>();
                var order = crafting.CurrentWorkOrder;
                var missing = order?.MissingIngredients.FirstOrDefault(x => this.Tanks.Where(y => x.Item.Type == y.Contents?.Item?.Type).Any());
                if (missing == null)
                {
                    return;
                }
                else
                {
                    LiquidTank tank = null;
                    foreach(LiquidTank l in this.Tanks)
                    {
                        if (l.Contents.Empty)
                            continue;
                        if(l.Contents.Item.Type.Equals(missing.Item.Type))
                        {
                            tank = l;
                            break;
                        }
                    }

                    if(tank==null)
                    {
                        return;
                    }


                    int missingAmount = missing.Quantity; //5
                    int dispo = (int) Math.Floor((decimal) (tank.Contents.Quantity / 1000));
                    int toTake = 0;
                    if (dispo<1)
                    {
                        return;
                    }

                    toTake = Math.Min(dispo, missingAmount);
                    if (toTake!=0)
                    {
                        DynamicInventory fakeInv = new DynamicInventory();
                        fakeInv.AddItems(tank.Contents.Item.Type,toTake);
                        var test = order.TryToContributeItems(order.Owner.Player, fakeInv.Stacks.First());

                        SkillJob.quantiyModfier.Invoke(tank.Contents, new Object[] { (tank.Contents.Quantity - (toTake*1000)  ) });
                    }

                  
                }
            }
        }
    }
}
