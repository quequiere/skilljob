﻿using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Pipes.LiquidComponents;
using Eco.Mods.TechTree;
using Eco.Shared.Math;
using Eco.Shared.Serialization;
using Eco.World;
using Eco.World.Blocks;
using SkillJob.SkillsTools.tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SkillJob.SkillsTools.Items.Table
{
    [Serialized]
    class ContinueLiquiProducer : LiquidProducer
    {

        private bool pumped = false;
        private int lastModulo = int.MaxValue;
        private int cooldown = 0;

        public ContinueLiquiProducer() { }
        public ContinueLiquiProducer(string name, Type itemType, int tankSize, Ray input, Ray output, float rateItemsPerSecond, float throughputItemsPerSecond = 5)
            : base(name, itemType, tankSize, input, output, throughputItemsPerSecond)
        {
            this.ItemPrototype = Item.Get(itemType);
            this.rateItemsPerSecond = rateItemsPerSecond;

        }
        public override void Tick(float delta)
        {
            base.Tick(delta);

            if (this.Contents == null)
                this.Contents = new ItemStack();

            SkillJob.itemModfier.Invoke(this.Contents, new Object[] { this.ItemPrototype });
            int modulo = this.Contents.Quantity % 1000;
            if (modulo < this.lastModulo)
            {
                DateTime now = DateTime.Now;


                if (cooldown < 100)
                {
                    cooldown++;
                    return;
                }


                if(pumpWater())
                {
                    pumped = true;
                }
                else
                {
                    pumped = false;
                    cooldown = 0;
                }
            }


            if (!pumped)
            {
                return;
            }

            this.lastModulo = modulo;


            if (this.owner.Enabled)
            {
                this.overflow += LiquidUtils.ItemsToQuantity(this.rateItemsPerSecond) * delta;
            }


            if (this.Contents.Quantity < this.TankSize)
            {
       
                var complete = Math.Min((int)this.overflow, this.TankSize - this.Contents.Quantity);
                this.overflow -= complete;
                if (complete > 0)
                    SkillJob.quantiyModfier.Invoke(this.Contents, new Object[] { this.Contents.Quantity + complete });
            }

        }


        private List<Type> possibleTypes = new List<Type> {typeof(DirtBlock), typeof(StoneBlock), typeof(SandBlock) };

        private Boolean pumpWater()
        {

            if (SkillJobTools.hasWater(this.owner.Position3i, 10, out Vector3i pos))
            {
                Console.WriteLine("remvoved:" + pos);

                Random rnd = new Random();
                int r = rnd.Next(possibleTypes.Count);

                World.SetBlock(possibleTypes[r], pos);

                return true;
            }

            return false;

        }
    }
}
