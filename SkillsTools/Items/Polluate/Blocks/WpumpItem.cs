﻿using Eco.Gameplay.Components;
using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using Eco.Shared.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkillJob.SkillsTools.Items.Table
{

    [Serialized]
    public class WpumpItem : WorldObjectItem<WpumpObject>
    {
        public override string FriendlyName { get { return "WpumpObject"; } }
        public override string Description { get { return "WpumpObject"; } }

        static WpumpItem()
        {

        }
    }

    public class WpumpRecipe : Recipe
    {
        public WpumpRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<WpumpItem>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<WaterItem>(1),
            };
            this.CraftMinutes = new ConstantValue(1);
            this.Initialize("SWater pump", typeof(WpumpRecipe));
            CraftingComponent.AddRecipe(typeof(WpumpObject), this);
        }
    }

    public class WpumpRecipeO : Recipe
    {
        public WpumpRecipeO()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<WpumpItem>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<StoneItem>(1),
            };
            this.CraftMinutes = new ConstantValue(1);
            this.Initialize("test", typeof(WpumpRecipeO));
            CraftingComponent.AddRecipe(typeof(WpumpObject), this);
        }
    }
}
