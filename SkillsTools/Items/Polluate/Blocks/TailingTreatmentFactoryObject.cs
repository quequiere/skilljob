﻿using Eco.Gameplay.Objects;
using Eco.Gameplay.Pipes.LiquidComponents;
using Eco.Shared.Serialization;
using Eco.Gameplay.Components;
using Eco.Gameplay.Components.Auth;
using Eco.Gameplay.Housing;
using Eco.Mods.TechTree;
using System;
using System.Collections.Generic;
using Eco.Gameplay.Pipes.Gases;
using Eco.Shared.Utils;
using Eco.Shared.Math;

namespace SkillJob.SkillsTools.Items.Table
{

    [Serialized]
    [RequireComponent(typeof(PipeComponent))]
    [RequireComponent(typeof(AttachmentComponent))]
    [RequireComponent(typeof(PropertyAuthComponent))]

    [RequireComponent(typeof(LinkComponent))]



    [RequireComponent(typeof(PowerGridComponent))]
    [RequireComponent(typeof(PowerConsumptionComponent))]

 


   // [RequireComponent(typeof(SolidGroundComponent))]
    public class TailingTreatmentFactoryObject : WorldObject
    {
        public override string FriendlyName { get { return "Tailing Treatment Factory"; } }




        protected override void Initialize()
        {
    
            var tankList = new List<LiquidTank>();
            //
            // tankList.Add(new LiquidPump("testha", typeof(WaterItem), 100, this.Occupancy.Find(x => x.Name == "ChimneyOut"), null, 1f));

            // tankList.Add(new LiquidTank("Water", typeof(WaterItem), 1000, Ray.Up,Ray.Right,1f));

            //LiquidPump l =  new LiquidPump("Water Pump", typeof(WaterItem), 1000, Ray.Left, null, 1f);
            //voir wireoutput


            //tankList.Add(new LiquidProducer("Chimney", typeof(SmogItem), 100, null, this.Occupancy.Find(x => x.Name == "ChimneyOut"),(float)(1 * SmogItem.SmogItemsPerCO2PPM) / TimeUtil.SecondsPerHour));



            tankList.Add(new LiquidProducer("Chimney", typeof(WaterItem), 1000, null, this.Occupancy.Find(x => x.Name == "ChimneyOut"), 1f));


            this.GetComponent<PipeComponent>().Initialize(tankList);



            this.GetComponent<PowerConsumptionComponent>().Initialize(400);
            this.GetComponent<PowerGridComponent>().Initialize(10, new ElectricPower());

            this.GetComponent<PropertyAuthComponent>().SetPublic(false);

       
        }
    }
}
