﻿using Eco.Gameplay.Items;
using Eco.Shared.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkillJob.SkillsTools.Items.Table
{

    [Serialized]
    public class TailingTreatmentFactoryItem : WorldObjectItem<TailingTreatmentFactoryObject>
    {
        public override string FriendlyName { get { return "Tailing Treatment Factory"; } }
        public override string Description { get { return "This factory can transform tailing"; } }

        static TailingTreatmentFactoryItem()
        {

        }
    }
}
