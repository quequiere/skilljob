﻿using Eco.Gameplay.Objects;
using Eco.Gameplay.Pipes.LiquidComponents;
using Eco.Shared.Serialization;
using Eco.Gameplay.Components;
using Eco.Gameplay.Components.Auth;
using Eco.Gameplay.Housing;
using Eco.Mods.TechTree;
using System;
using System.Collections.Generic;
using Eco.Gameplay.Pipes.Gases;
using Eco.Shared.Utils;
using Eco.Shared.Math;

namespace SkillJob.SkillsTools.Items.Table
{

    [Serialized]
    [RequireComponent(typeof(PipeCompoCustom))]

    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(LinkComponent))]
    [RequireComponent(typeof(PowerGridComponent))]
    [RequireComponent(typeof(PowerConsumptionComponent))]


    [RequireComponent(typeof(CraftingComponent))]
    [RequireComponent(typeof(OnOffComponent))]

    [RequireComponent(typeof(SolidGroundComponent))]
    public class WpumpObject : WorldObject
    {
        public override string FriendlyName => "WpumpObject";


        protected override void Initialize()
        {
            var tankList = new List<LiquidTank>
            {
                new ContinueLiquiProducer("Water", typeof(WaterItem), 1000, null, null, 0.1f)
            };

            this.GetComponent<PipeCompoCustom>().Initialize(tankList);
            this.GetComponent<PowerConsumptionComponent>().Initialize(400);
            this.GetComponent<PowerGridComponent>().Initialize(10, new ElectricPower());

            this.GetComponent<PropertyAuthComponent>().SetPublic(false);

        }
    }
}
