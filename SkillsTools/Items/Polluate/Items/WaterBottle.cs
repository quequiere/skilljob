﻿using Eco.Gameplay.Components;
using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using Eco.Shared.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkillJob.SkillsTools.Items.Table
{

    [Serialized]
    public class WaterBottle : Item
    {
        public override string FriendlyName { get { return "WaterBottle"; } }
        public override string Description { get { return "Water Bottle"; } }

        static WaterBottle()
        {

        }
    }

    public class WaterBottleRecipe : Recipe
    {
        public WaterBottleRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<WaterBottle>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<WaterItem>(1),
                new CraftingElement<EmptyBottle>(1),
            };
            this.CraftMinutes = new ConstantValue(1);
            this.Initialize("Water Bottle", typeof(WaterBottleRecipe));
            CraftingComponent.AddRecipe(typeof(WpumpObject), this);
        }
    }

}
