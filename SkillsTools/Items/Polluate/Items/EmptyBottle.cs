﻿using Eco.Gameplay.Components;
using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using Eco.Shared.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkillJob.SkillsTools.Items.Table
{

    [Serialized]
    public class EmptyBottle : Item
    {
        public override string FriendlyName { get { return "EmptyBottle"; } }
        public override string Description { get { return "Empty Bottle"; } }

        static EmptyBottle()
        {

        }
    }

    public class EmptyBottleRecipe : Recipe
    {
        public EmptyBottleRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<EmptyBottle>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<PlasticItem>(2),
            };
            this.CraftMinutes = new ConstantValue(2);
            this.Initialize("Empty Bottle", typeof(EmptyBottleRecipe));
            CraftingComponent.AddRecipe(typeof(TailingTreatmentFactoryObject), this);
        }
    }


    public class TailingDestruction : Recipe
    {
        public TailingDestruction()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<EmptyBottle>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<TailingsItem>(1),
            };
            this.CraftMinutes = new ConstantValue(5);
            this.Initialize("Tailing Destruction", typeof(EmptyBottleRecipe));
            CraftingComponent.AddRecipe(typeof(TailingTreatmentFactoryObject), this);
        }
    }

}
