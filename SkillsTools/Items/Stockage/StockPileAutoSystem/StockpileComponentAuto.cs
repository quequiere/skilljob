﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eco.Gameplay.Objects;
using Eco.Shared.Math;
using Eco.Shared.Serialization;
using Eco.Shared.Utils;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.World;
using Eco.World.Blocks;

namespace SkillJob.SkillsTools.Items.Stockage
{


    [Serialized]
    [RequireComponent(typeof(PublicStorageComponent))]
    public class StockpileComponentAuto : WorldObjectComponent
    {
        public static readonly Vector3i Dimensions = new Vector3i(10, 10, 10);
        PublicStorageComponent storage;
        double lastScanTime = double.MinValue;
        bool skipWorldSync;
        bool updatingFromInventory;

        public StockpileComponentAuto()
        { }

        public override void Initialize()
        {

            this.storage = this.Parent.GetComponent<PublicStorageComponent>();
            this.storage.Storage.OnChanged.Add((user) => this.UpdateStockpileFromInventory());
            this.storage.Storage.AddInvRestriction(new StockpileStackRestriction(Dimensions.y)); // limit stack sizes to the y-height of the stockpile

            // delete any items in the stockpile that are no longer block items
            this.storage.Storage.Modify(changeSet =>
            {
                // PLASTIC MIGRATION, REMOVE ME
                var plastic = Item.Get("PlasticItem");
                foreach (var stack in this.storage.Storage.NonEmptyStacks)
                {
                    if (!(stack.Item is BlockItem) && stack.Item != plastic) // remove plastic check
                        changeSet.RemoveItems(stack.Item.Type, stack.Quantity);
                }
            });

            // world object blocks are set on load, so set all the blocks according to the inventory so they match
            this.UpdateStockpileFromInventory();
        }

        public override void OnCreate()
        {
            base.OnCreate();

            // spawn some fx!
            Vector3i position = this.Parent.Position3i - new Vector3i((int)(Dimensions.x / 2f), 1, (int)(Dimensions.z / 2f));
            foreach (var offset in Dimensions.XZ.XYIter())
            {
                // delete all the grass/debris at the ground level, turn blocks underneath to dirt, if its fertile soil (world layers may reclaim it back later)
                var worldPos = position + offset.X_Z();
                if (World.GetBlock(worldPos).Is<Fertile>())
                    World.SetBlock<DirtBlock>(worldPos);
                this.Parent.Creator.Player?.SpawnBlockEffect(worldPos + Vector3i.Up, typeof(DirtBlock), BlockEffect.Place);
            }
        }

        public override void Tick()
        {
            this.TryUpdateInventoryFromWorld();
        }

        private void TryUpdateInventoryFromWorld()
        {
            if (this.updatingFromInventory)
                return;

            // see if any chunks in our domain changed, if so, rescan the available quantities
            Vector3i position = this.Parent.Position3i - new Vector3i((int)(Dimensions.x / 2f), 0, (int)(Dimensions.z / 2f));
            var chunks = new PersistentChunk[]
            {
                #pragma warning disable SA1008 // Opening parenthesis must be spaced correctly
                World.GetChunk(World.ToChunkPosition(position + new Vector3i(           0,            0,            0))),
                World.GetChunk(World.ToChunkPosition(position + new Vector3i(Dimensions.x,            0,            0))),
                World.GetChunk(World.ToChunkPosition(position + new Vector3i(           0, Dimensions.y,            0))),
                World.GetChunk(World.ToChunkPosition(position + new Vector3i(Dimensions.x, Dimensions.y,            0))),
                World.GetChunk(World.ToChunkPosition(position + new Vector3i(           0,            0, Dimensions.z))),
                World.GetChunk(World.ToChunkPosition(position + new Vector3i(Dimensions.x,            0, Dimensions.z))),
                World.GetChunk(World.ToChunkPosition(position + new Vector3i(           0, Dimensions.y, Dimensions.z))),
                World.GetChunk(World.ToChunkPosition(position + new Vector3i(Dimensions.x, Dimensions.y, Dimensions.z))),
                #pragma warning restore SA1008 // Opening parenthesis must be spaced correctly
            };

            var lastModified = chunks.NonNull().MaxOrDefault(chunk => chunk.LastUpdated);
            if (lastModified > this.lastScanTime)
            {
                // get number of things in this space
                Dictionary<Type, int> itemQuantities = new Dictionary<Type, int>();
                foreach (var worldPos in Dimensions.YXZIter())
                {
                    var block = World.GetBlock(position + worldPos);
                    if (block is WaterBlock)
                        continue; // special case ignore water blocks

                    var item = BlockItem.CreatingItem(block.GetType());
                    if (item != null)
                    {
                        int quantity;
                        itemQuantities.TryGetValue(item.Type, out quantity);

                        var blockTypeIndex = Array.IndexOf(item.BlockTypes, block.GetType());
                        if (blockTypeIndex == -1)
                            blockTypeIndex = 0; // grass blocks aren't in the dirtitems block list, but we still need to add one quantity

                        int addQuantity = blockTypeIndex + 1;
                        quantity += addQuantity;
                        itemQuantities[item.Type] = quantity;
                    }
                }

                this.lastScanTime = lastModified;

                this.skipWorldSync = true;
                var result = this.storage.Storage.TryModify(changeSet =>
                {
                    foreach (var itemTypePair in itemQuantities)
                    {
                        Type itemType = itemTypePair.Key;
                        int numShouldHave = itemTypePair.Value;
                        int numHave = this.storage.Storage.TotalNumberOfItems(itemType);
                        if (numHave != numShouldHave)
                        {
                            // adjust quantities of the inventory to match what is displayed in the world
                            if (numHave > numShouldHave)
                                changeSet.RemoveItems(itemType, numHave - numShouldHave);
                            else if (numHave < numShouldHave)
                                changeSet.AddItems(itemType, numShouldHave - numHave);
                        }
                    }
                });
                if (!result.Success)
                    Log.WriteError("Failed to sync stockpile inventory with world contents.");
                this.skipWorldSync = false;
            }





        }

        private class StockInfo
        {
            public Item Item;
            public int Remaining;
            public float Allotment;
        }

        void UpdateStockpileFromInventory()
        {
            if (this.skipWorldSync)
                return;

            this.updatingFromInventory = true;
            this.storage.Storage.Modify(changeSet =>
            {
                // update blocks based on new inventory
                Vector3i position = this.Parent.Position3i - new Vector3i((int)(Dimensions.x / 2f), 0, (int)(Dimensions.z / 2f));

                int totalStacks = this.storage.Storage.NonEmptyStacks.Count();
                var stockInfo = this.storage.Storage.NonEmptyStacks.GroupBy(stack => stack.Item.Type)
                    .Select(group => new StockInfo() { Item = group.First().Item, Remaining = group.Sum(stack => stack.Quantity), Allotment = (float)group.Count() / totalStacks })
                    .OrderByDescending(info => info.Allotment).ToArray();

                float allotment = 0;
                for (int i = 0; i < stockInfo.Length; i++)
                {
                    var allotmentCurrent = stockInfo[i].Allotment;
                    stockInfo[i].Allotment += allotment;
                    allotment += allotmentCurrent;
                }

                for (int y = 0; y < Dimensions.y; y++)
                {
                    int currentStackPos = 0;
                    int stockIdx = 0;
                    for (int z = 0; z < Dimensions.z; z++)
                    {
                        for (int x = 0; x < Dimensions.x; x++)
                        {
                            Type placedBlock = null;
                            if (stockIdx < stockInfo.Length)
                            {
                                var info = stockInfo[stockIdx];
                                if (info.Remaining > 0)
                                {
                                    var blockItem = info.Item as BlockItem;
                                    if (blockItem == null) // remove once plastic is fixed
                                        continue;
                                    int idx = Math.Min(info.Remaining - 1, blockItem.BlockTypes.Length - 1);
                                    info.Remaining -= idx + 1;
                                    placedBlock = blockItem.BlockTypes[idx];
                                }

                                currentStackPos++;
                                if ((float)currentStackPos / (Dimensions.x * Dimensions.z) >= info.Allotment)
                                    stockIdx++;
                            }
                            if (placedBlock != null)
                                World.SetBlock(placedBlock, position + new Vector3i(x, y, z));
                            else
                                World.SetBlock<WorldObjectBlock>(position + new Vector3i(x, y, z), this.Parent);
                        }
                    }
                }

                // reset the scan time
                this.lastScanTime = TimeUtil.Seconds;
            });

            this.updatingFromInventory = false;
        }



    }
  
}
