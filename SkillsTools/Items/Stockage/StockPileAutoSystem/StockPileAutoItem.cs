﻿using Eco.Gameplay.Components;
using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using Eco.Shared.Localization;
using Eco.Shared.Math;
using Eco.Shared.Serialization;
using Eco.World;
using Eco.World.Blocks;
using SkillJob.SkillsTools.SkillWorkerJob.StorageMasterBase.Skills;


namespace SkillJob.SkillsTools.Items.Stockage.Test
{
    [Serialized]
    public partial class StockPileAutoItem : WorldObjectItem<StockPileAutoObject>
    {
        public override string FriendlyName { get { return "StockPileAuto Item"; } }
        public override string Description { get { return "StockPile automatised"; } }

        static StockPileAutoItem()
        {

        }

        public override bool TryPlaceObject(Player player, Vector3i position, Quaternion rotation)
        {

            Vector3i startPosition = position - new Vector3i((int)(StockpileComponentAuto.Dimensions.x / 2f), 1, (int)(StockpileComponentAuto.Dimensions.z / 2f));
            foreach (var offset in StockpileComponentAuto.Dimensions.XZ.XYIter())
            {
                var worldPos = startPosition + offset.X_Z();
                if (!World.GetBlock(worldPos).Is<Solid>())
                {
                    player.SendTemporaryErrorLoc("Stockpile requires solid ground to be placed on.");
                    return false;
                }
            }
            return base.TryPlaceObject(player, position, rotation);
        }


    }

    [RequiresSkill(typeof(StorageConstruction), 1)]
    public partial class StockPileAutoRecipe : Recipe
    {
        public StockPileAutoRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<StockPileAutoItem>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<CircuitItem>(typeof(StorageEfficiency), (3*100)/20, StorageEfficiency.MultiplicativeStrategy),
                new CraftingElement<CelluloseFiberItem>(typeof(StorageEfficiency), (50*100)/20, StorageEfficiency.MultiplicativeStrategy),
                new CraftingElement<AdvancedCombustionEngineItem>(typeof(StorageEfficiency), (1*100)/20, StorageEfficiency.MultiplicativeStrategy),
            };
            SkillModifiedValue value = new SkillModifiedValue((10 * 100) / 20, StorageSpeed.MultiplicativeStrategy, typeof(StorageSpeed), Localizer.DoStr("craft time"));
            SkillModifiedValueManager.AddBenefitForObject(typeof(StockPileAutoRecipe), Localizer.DoStr("test1"), value);
            SkillModifiedValueManager.AddSkillBenefit(Localizer.DoStr(""), value);
            this.CraftMinutes = value;
            this.Initialize("Automated stockpile", typeof(StockPileAutoRecipe));
            CraftingComponent.AddRecipe(typeof(RoboticAssemblyLineObject), this);
        }
    }
}
