﻿using Eco.Gameplay.Objects;
using Eco.Shared.Math;
using System.Collections.Generic;

namespace SkillJob.SkillsTools.Items.Stockage.Test
{
    public class StockPileAutoOccupency
    {
        public static void register()
        {
            List<BlockOccupancy> liste = new List<BlockOccupancy>();

            int xmin = -5;
            int ymin = 0;
            int zmin = -5;

            int xmax = 5;
            int ymax = 10;
            int zmax = 5;

            for (int x = xmin; x <= xmax; x++)
            {
                for (int y = ymin; y <= ymax; y++)
                {
                    for (int z = zmin; z <= zmax; z++)
                    {
                        liste.Add(new BlockOccupancy(new Vector3i(x, y, z)));
                    }
                }
            }

            WorldObject.AddOccupancy<StockPileAutoObject>(liste);


        }
    }
}
