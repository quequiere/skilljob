﻿using Eco.Gameplay.Components;
using Eco.Gameplay.Components.Auth;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Shared;
using Eco.Shared.Math;
using Eco.Shared.Networking;
using Eco.Shared.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SkillJob.SkillsTools.Items.Stockage.Test
{

    [Serialized]
    [RequireComponent(typeof(PowerGridComponent))]
    [RequireComponent(typeof(PowerConsumptionComponent))]
    [RequireComponent(typeof(OnOffComponent))]
    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(PublicStorageComponent))]
    [RequireComponent(typeof(StockpileComponentAuto))]
    [RequireComponent(typeof(LinkComponent))]
    public partial class StockPileAutoObject : WorldObject, IRepresentsItem
    {
        private static int refreshTime = 3;
        private static int energyConsomation = 400;
        private static int range = 30;

        public override string FriendlyName { get { return "StockPileAutoObject"; } }
        public virtual Type RepresentedItemType { get { return typeof(StockPileAutoItem); } }
        private DateTime lastTime = DateTime.Now;


        protected override void Initialize()
        {
            base.Initialize();
            this.GetComponent<PowerConsumptionComponent>().Initialize(energyConsomation);
            this.GetComponent<PowerGridComponent>().Initialize(10, new ElectricPower());



            var storage = this.GetComponent<PublicStorageComponent>();
            storage.Initialize(StockpileComponentAuto.Dimensions.x * StockpileComponentAuto.Dimensions.z);
            storage.Storage.AddInvRestriction(new CarriedRestriction()); 
            this.GetComponent<PropertyAuthComponent>().SetPublic(false);
            this.GetComponent<LinkComponent>().Initialize(range);


        }

        public override void SendInitialState(BSONObject bsonObj, INetObjectViewer viewer)
        {
            base.SendInitialState(bsonObj, viewer);
            bsonObj["noFadeIn"] = true;
        }

        public override void Tick()
        {
            this.tryToTakeItemsFromOtherIventory();
        }


        //quequiere add
        public void tryToTakeItemsFromOtherIventory()
        {
            DateTime now = DateTime.Now;


            
            if (now.Subtract(lastTime).TotalSeconds < refreshTime)
            {
                return;
            }
            lastTime = now;
            if (this.OwnerUser == null)
                return;

            OnOffComponent statut = this.GetComponent<OnOffComponent>();
            if (!statut.On)
            {
                return;
            }

            PowerGridComponent power = this.GetComponent<PowerGridComponent>();
            PowerConsumptionComponent pc = this.GetComponent<PowerConsumptionComponent>();


            float dispo = power.PowerGrid.EnergySupply - power.PowerGrid.EnergyDemand;
            if (dispo<0)
            {
                return;
            }
           


            PublicStorageComponent localStorage = this.GetComponent<PublicStorageComponent>();
            LinkComponent link = this.GetComponent<LinkComponent>();
            IEnumerable<StorageComponent> col = link.GetSortedLinkedComponents(this.OwnerUser);
            if(localStorage.Inventory.Stacks.Where(stack => stack.Quantity == 0).Count()>0)
            {
                //permettra a l'avenir de check les itemstack non complets
                bool hasTransfered = false;
                if (tryToTransfert(col, localStorage.Inventory))
                {
                    hasTransfered = true;
                }

                if(!hasTransfered)
                {
                    if(tryToGetFromWorld())
                    {
                        hasTransfered = true;
                    }
                }
            }

         
        }


        public bool tryToGetFromWorld()
        {

            return false;
        }

        public ArrayList getObjects()
        {
            ArrayList liste = new ArrayList();

            foreach (WorldObject obj in WorldObjectManager.All)
            {
                float distance = Vector3.Distance(obj.Position, this.Position);
                distance = Mathf.Round(distance * 100f) / 100f;

                if (distance <= range)
                {
                    liste.Add(obj);
                }
            }

            return liste;
        }

        public bool tryToTransfert(IEnumerable<StorageComponent> col, Inventory dest)
        {
            foreach (StorageComponent compo in col)
            {
                if (!(compo is StorageComponent) || compo is FuelSupplyComponent)
                {
                    ////Console.WriteLine("Eliminated: " + compo.GetType());
                    continue;
                }

                StorageComponent psc = compo as StorageComponent;
                if (cantHaveCaried(psc))
                {
                    //Console.WriteLine("Eliminated2: " + compo.Parent.GetType());
                    continue;
                }

                if (psc.Inventory.IsEmpty)
                {
                    // Console.WriteLine("Eliminated3: " + compo.Parent.GetType());
                    continue;
                }

              


                if(psc.Parent is StockPileAutoObject)
                {
                    if (psc.Parent.OwnerUser != null)
                    {
                        OnOffComponent statut = psc.Parent.GetComponent<OnOffComponent>();
                        if (statut.On)
                        {
                            continue;
                        }
                 
                    }
                   
                }

               //  Console.WriteLine("okscan: " + compo.Parent.GetType());

                ItemStack toDeplace = null;

                foreach (ItemStack ist in psc.Inventory.NonEmptyStacks.Where(stack => stack.Item.IsCarried ))
                {
                    toDeplace = ist;
                    break;
                }

                if(toDeplace==null)
                {
                    //// Console.WriteLine("FailToDeplace: " + compo.Parent.GetType());
                    continue;
                }

                ItemStack newis =  new ItemStack(toDeplace.Item, toDeplace.Quantity);
                psc.Inventory.RemoveItems(toDeplace);
                dest.AddItems(newis);
                return true;

            }
            return false;
        }


        public bool cantHaveCaried(StorageComponent psc)
        {
            if(psc is PublicStorageComponent)
            {
                PublicStorageComponent pscc = psc as PublicStorageComponent;
                foreach (InventoryRestriction restriction in pscc.Storage.Restrictions)
                {
                    if (restriction is NotCarriedRestriction)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

    }




 



}
