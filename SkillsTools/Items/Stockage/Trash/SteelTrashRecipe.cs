﻿using Eco.Gameplay.Components;
using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using Eco.Shared.Localization;
using SkillJob.SkillsTools.SkillWorkerJob.StorageMasterBase.Skills;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkillJob.SkillsTools.Items.Stockage.Trash
{

    [RequiresSkill(typeof(StorageConstruction), 1)]
    public partial class SteelTrashRecipe : Recipe
    {
        public SteelTrashRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<SteelTrashItem>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<PlasticItem>(typeof(StorageEfficiency), (1*100)/20, StorageEfficiency.MultiplicativeStrategy),
                new CraftingElement<IronIngotItem>(typeof(StorageEfficiency), (5*100)/20, StorageEfficiency.MultiplicativeStrategy),
            };
            SkillModifiedValue value = new SkillModifiedValue((10 * 100) / 20, StorageSpeed.MultiplicativeStrategy, typeof(StorageSpeed), Localizer.DoStr("craft time"));
            SkillModifiedValueManager.AddBenefitForObject(typeof(SteelTrashRecipe), Localizer.DoStr("test111"), value);
            SkillModifiedValueManager.AddSkillBenefit(Localizer.DoStr(""), value);
            this.CraftMinutes = value;
            this.Initialize("Trash recipe", typeof(SteelTrashRecipe));
            CraftingComponent.AddRecipe(typeof(WorkbenchObject), this);
        }
    }
}
