﻿
using System;
using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Shared.Math;
using Eco.Shared.Serialization;
using System.Linq;
using Eco.Gameplay.Components.Auth;

namespace SkillJob.SkillsTools.Items.Stockage.Trash
{


    [Serialized]
    [RequireComponent(typeof(PowerGridComponent))]
    [RequireComponent(typeof(PowerConsumptionComponent))]
    [RequireComponent(typeof(PublicStorageComponent))]
    [RequireComponent(typeof(PropertyAuthComponent))]
    public partial class SteelTrashObject : WorldObject, IRepresentsItem
    {
        private static int refreshTime = 60;
        private static int energyConsomation = 300;


        public override string FriendlyName { get { return "Public trash"; } }
        public virtual Type RepresentedItemType { get { return typeof(SteelTrashItem); } }

        private DateTime lastTime = DateTime.Now;

        protected override void Initialize()
        {

            base.Initialize();


            this.GetComponent<PowerConsumptionComponent>().Initialize(energyConsomation);
            this.GetComponent<PowerGridComponent>().Initialize(10, new ElectricPower());

            var storage = this.GetComponent<PublicStorageComponent>();
            storage.Initialize(4);
            storage.Storage.AddInvRestriction(new NotCarriedRestriction());
        }

        protected override void PostInitialize()
        {
            base.PostInitialize();
            this.GetComponent<PropertyAuthComponent>().SetPublic(true);

        }

        public override void Destroy()
        {
            base.Destroy();
        }



        public override void Tick()
        {
            this.tryToRemoveItems();
        }

        public void tryToRemoveItems()
        {
            DateTime now = DateTime.Now;
            if (now.Subtract(lastTime).TotalSeconds < refreshTime)
            {
                return;
            }

            lastTime = now;


            PowerGridComponent power = this.GetComponent<PowerGridComponent>();
            PowerConsumptionComponent pc = this.GetComponent<PowerConsumptionComponent>();


            float dispo = power.PowerGrid.EnergySupply - power.PowerGrid.EnergyDemand;
            if (dispo < 0)
            {
                return;
            }

            PublicStorageComponent localStorage = this.GetComponent<PublicStorageComponent>();
            if (localStorage.Inventory.Stacks.Where(stack => stack.Quantity != 0).Count() > 0)
            {
                Inventory inv = localStorage.Inventory;
                ItemStack toremove = null;
                foreach (ItemStack ist in inv.NonEmptyStacks)
                {
                    toremove = ist;
                    break;
                }

                if(toremove!=null)
                {
                    inv.RemoveItems(toremove.Item.Type, 1, null);
                }

            }
        }

    }






}
