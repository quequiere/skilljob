﻿using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Chat;
using Eco.Shared;
using Eco.Shared.Math;
using Eco.Shared.Serialization;
using Eco.Shared.Services;
using SkillJob.SkillsTools.Items.Stockage.Test;


namespace SkillJob.SkillsTools.Items.Stockage.Trash
{
    [Serialized]
    public partial class SteelTrashItem : WorldObjectItem<SteelTrashObject>
    {
        private static int minimumDistance = 20;

        public override string FriendlyName { get { return "Steel trash"; } }
        public override string Description { get { return "A trash that destroy object inside with lot of time and energy"; } }

        static SteelTrashItem()
        {

        }

        public override bool TryPlaceObject(Player player, Vector3i position, Quaternion rotation)
        {
            foreach (WorldObject obj in WorldObjectManager.All)
            {
                if (obj is SteelTrashObject)
                {
                    float distance = Vector3.Distance(obj.Position, player.User.Position);
                    distance = Mathf.Round(distance * 100f) / 100f;

                    if (distance <= minimumDistance)
                    {
                        ChatManager.ServerMessageToPlayerAlreadyLocalized(SkillJob.coloredPrefix + "You need more space between two trash: " + minimumDistance + " meters.", player.User, true, DefaultChatTags.General);
                        return false;
                    }
                }

            }
            return base.TryPlaceObject(player, position, rotation); ;
        }
    }
}
