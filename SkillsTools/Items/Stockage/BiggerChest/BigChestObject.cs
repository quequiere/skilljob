﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Skills;
using Eco.Shared.Math;
using Eco.Shared.Localization;
using Eco.Shared.Serialization;
using Eco.Mods.TechTree;
using SkillJob.SkillsTools.SkillWorkerJob.StorageMasterBase.Skills;
using Eco.Gameplay.Components.Auth;
using System;

namespace SkillJob.SkillsTools.Items.Stockage.BiggerChest
{


    [Serialized]
    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(MinimapComponent))]
    [RequireComponent(typeof(LinkComponent))]
    [RequireComponent(typeof(PublicStorageComponent))]
    public partial class BigChestObject : WorldObject,IRepresentsItem
    {
        public override string FriendlyName { get { return "Big Chest"; } }

        public virtual Type RepresentedItemType { get { return typeof(BigChestItem); } }


        protected override void Initialize()
        {
            this.GetComponent<MinimapComponent>().Initialize("Storage");
            var storage = this.GetComponent<PublicStorageComponent>();
            storage.Initialize(16*4);
            storage.Storage.AddInvRestriction(new NotCarriedRestriction()); 
        }

        protected override void PostInitialize()
        {
            base.PostInitialize();
            this.GetComponent<PropertyAuthComponent>().SetPublic(false);
            this.GetComponent<LinkComponent>().Initialize(5);
            this.GetComponent<MinimapComponent>().Initialize("Storage");
        }

        public override void Destroy()
        {
            base.Destroy();
        }

    }

    [Serialized]
    public partial class BigChestItem :WorldObjectItem<BigChestObject>
    {
        public override string FriendlyName { get { return "Big Chest"; } }
        public override string Description { get { return "A container you can store items in."; } }

        static BigChestItem()
        {

        }


    }



    [RequiresSkill(typeof(StorageConstruction), 1)]
    public partial class BigChestRecipe : Recipe
    {
        public BigChestRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<BigChestItem>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<LumberItem>(typeof(StorageEfficiency), (30*100)/20, StorageEfficiency.MultiplicativeStrategy),
                new CraftingElement<IronIngotItem>(typeof(StorageEfficiency), (10*100)/20, StorageEfficiency.MultiplicativeStrategy),
            };
            SkillModifiedValue value = new SkillModifiedValue((10 * 100) / 20, StorageSpeed.MultiplicativeStrategy, typeof(StorageSpeed), Localizer.DoStr("craft time"));
            SkillModifiedValueManager.AddBenefitForObject(typeof(BigChestRecipe), Localizer.DoStr("test11"), value);
            SkillModifiedValueManager.AddSkillBenefit(Localizer.DoStr(""), value);
            this.CraftMinutes = value;
            this.Initialize("Big chest recipe", typeof(BigChestRecipe));
            CraftingComponent.AddRecipe(typeof(SawmillObject), this);
        }
    }



}
