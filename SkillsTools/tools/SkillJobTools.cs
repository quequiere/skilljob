﻿using Eco.Gameplay.Items;
using Eco.Mods.TechTree;
using Eco.Shared.Math;
using Eco.World;
using Eco.World.Blocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkillJob.SkillsTools.tools
{
    public class SkillJobTools
    {
        public static List<Vector3i> getshpere(Vector3i originalPos, int range)
        {
            List < Vector3i > list = new List<Vector3i>();


            for (int y = -range; y <= range; y++)
            {
                for (int x = -range; x <= range; x++)
                {
                    for (int z = -range; z <= range; z++)
                    {
                        Vector3i toAdd = new Vector3i(x + originalPos.X, y + originalPos.Y, z + originalPos.Z);
                        list.Add(toAdd);
                    }

                }
            }

            return list;
        }


        public static Boolean hasWater(Vector3i originalPos,int range, out Vector3i pos )
        {

            pos = new Vector3i(0,0,0);
            List<Vector3i> list = getshpere(originalPos, range);

            foreach (Vector3i v in list)
            {
                Block b = World.GetBlock(v);
                Type targetTye = b.GetType();
                if (targetTye.Equals(typeof(WaterBlock)))
                {
                    WaterBlock w = b as WaterBlock;
                    if (w.Water>=1)
                    {
                        pos = v;
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
