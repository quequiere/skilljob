﻿using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils;
using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using EcoColorLib;
using SkillJob.SkillsTools.Items.Stockage.Test;
using SkillJob.SkillsTools.SkillWorkerJob.PillMakerBase;
using System;
using System.Reflection;

namespace SkillJob
{
    public class SkillJob : IModKitPlugin, IServerPlugin
    {
        public static String coloredPrefix = ChatFormat.Green.Value + ChatFormat.Bold.Value + prefix + ChatFormat.Clear.Value;
        public static String prefix = "SkillJob: ";

        public static MethodInfo quantiyModfier;
        public static MethodInfo itemModfier;

        public SkillJob()
        {
            initialzeCustomAcess();
        }

        public string GetStatus()
        {
            return String.Empty;
        }

        public void Initialize(TimedTask timer)
        {
            Console.WriteLine(SkillJob.prefix + " starting !");

            UserManager.OnUserLoggedIn.Add(u => {
                if (!u.Skillset.HasSkill(typeof(SkillWorker)))
                    u.Skillset.LearnSkill(typeof(SkillWorker));
            });

            StockPileAutoOccupency.register();
        }


        public void initialzeCustomAcess()
        {
            Console.WriteLine(SkillJob.prefix + " private method get !!!");


            if (quantiyModfier == null)
            {
                foreach (MethodInfo m in typeof(ItemStack).GetMethods(BindingFlags.Instance | BindingFlags.NonPublic))
                {
                    if (m.Name.Contains("set_Quantity"))
                    {
                        quantiyModfier = m;
                    }
                    else if (m.Name.Contains("set_Item"))
                    {
                        itemModfier = m;
                    }
                }
            }

        }

    }
}
